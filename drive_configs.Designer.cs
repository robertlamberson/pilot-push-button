﻿namespace PilotPushButton
{
    partial class drive_configs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(drive_configs));
            this.lstBxDriveNames = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddToList = new System.Windows.Forms.Button();
            this.txtNewName = new System.Windows.Forms.TextBox();
            this.btnSvCls = new System.Windows.Forms.Button();
            this.btnSv = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSaveFolder = new System.Windows.Forms.Button();
            this.txtDestFolder = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstBxDestDirs = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstBxDriveNames
            // 
            this.lstBxDriveNames.FormattingEnabled = true;
            this.lstBxDriveNames.Location = new System.Drawing.Point(11, 46);
            this.lstBxDriveNames.Margin = new System.Windows.Forms.Padding(2);
            this.lstBxDriveNames.Name = "lstBxDriveNames";
            this.lstBxDriveNames.Size = new System.Drawing.Size(442, 134);
            this.lstBxDriveNames.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnAddToList);
            this.groupBox1.Controls.Add(this.txtNewName);
            this.groupBox1.Controls.Add(this.lstBxDriveNames);
            this.groupBox1.Location = new System.Drawing.Point(8, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(465, 193);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Target Source Directories";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Add Directory Name";
            // 
            // btnAddToList
            // 
            this.btnAddToList.Location = new System.Drawing.Point(353, 13);
            this.btnAddToList.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddToList.Name = "btnAddToList";
            this.btnAddToList.Size = new System.Drawing.Size(98, 23);
            this.btnAddToList.TabIndex = 2;
            this.btnAddToList.Text = "Add Drive Name";
            this.btnAddToList.UseVisualStyleBackColor = true;
            this.btnAddToList.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtNewName
            // 
            this.txtNewName.Location = new System.Drawing.Point(181, 16);
            this.txtNewName.Margin = new System.Windows.Forms.Padding(2);
            this.txtNewName.Name = "txtNewName";
            this.txtNewName.Size = new System.Drawing.Size(170, 20);
            this.txtNewName.TabIndex = 1;
            // 
            // btnSvCls
            // 
            this.btnSvCls.Location = new System.Drawing.Point(361, 386);
            this.btnSvCls.Margin = new System.Windows.Forms.Padding(2);
            this.btnSvCls.Name = "btnSvCls";
            this.btnSvCls.Size = new System.Drawing.Size(112, 48);
            this.btnSvCls.TabIndex = 2;
            this.btnSvCls.Text = "Save && &Close";
            this.btnSvCls.UseVisualStyleBackColor = true;
            // 
            // btnSv
            // 
            this.btnSv.Location = new System.Drawing.Point(249, 386);
            this.btnSv.Margin = new System.Windows.Forms.Padding(2);
            this.btnSv.Name = "btnSv";
            this.btnSv.Size = new System.Drawing.Size(108, 48);
            this.btnSv.TabIndex = 3;
            this.btnSv.Text = "&Save";
            this.btnSv.UseVisualStyleBackColor = true;
            this.btnSv.Click += new System.EventHandler(this.btnSv_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSaveFolder);
            this.groupBox2.Controls.Add(this.txtDestFolder);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lstBxDestDirs);
            this.groupBox2.Location = new System.Drawing.Point(8, 208);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(465, 174);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Destination Directories";
            // 
            // btnSaveFolder
            // 
            this.btnSaveFolder.Location = new System.Drawing.Point(307, 22);
            this.btnSaveFolder.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveFolder.Name = "btnSaveFolder";
            this.btnSaveFolder.Size = new System.Drawing.Size(128, 23);
            this.btnSaveFolder.TabIndex = 4;
            this.btnSaveFolder.Text = "Use Destination Folder";
            this.btnSaveFolder.UseVisualStyleBackColor = true;
            this.btnSaveFolder.Click += new System.EventHandler(this.btnSaveFolder_Click);
            // 
            // txtDestFolder
            // 
            this.txtDestFolder.Location = new System.Drawing.Point(133, 28);
            this.txtDestFolder.Margin = new System.Windows.Forms.Padding(2);
            this.txtDestFolder.Name = "txtDestFolder";
            this.txtDestFolder.Size = new System.Drawing.Size(171, 20);
            this.txtDestFolder.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Add Destination";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 58);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Add Destination Directory";
            // 
            // lstBxDestDirs
            // 
            this.lstBxDestDirs.FormattingEnabled = true;
            this.lstBxDestDirs.Location = new System.Drawing.Point(4, 79);
            this.lstBxDestDirs.Margin = new System.Windows.Forms.Padding(2);
            this.lstBxDestDirs.Name = "lstBxDestDirs";
            this.lstBxDestDirs.Size = new System.Drawing.Size(449, 82);
            this.lstBxDestDirs.TabIndex = 0;
            // 
            // drive_configs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 453);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSv);
            this.Controls.Add(this.btnSvCls);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "drive_configs";
            this.Text = "Drive Configuration";
            this.Load += new System.EventHandler(this.drive_configs_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstBxDriveNames;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddToList;
        private System.Windows.Forms.TextBox txtNewName;
        private System.Windows.Forms.Button btnSvCls;
        private System.Windows.Forms.Button btnSv;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstBxDestDirs;
        private System.Windows.Forms.Button btnSaveFolder;
        private System.Windows.Forms.TextBox txtDestFolder;
        private System.Windows.Forms.Label label4;
    }
}