﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PilotPushButton
{
    public partial class conf_trans : Form
    {
        private int source_num;
        private string trans_phase;
        private string title_text;
        private string message_text;

        

        public conf_trans(int source_number, string transfer_phase, string title, string message)
        {
            InitializeComponent();

            this.source_num = source_number;
            this.trans_phase = transfer_phase;
            this.title_text = title;
            this.message_text = message;

        }

        private void conf_trans_Load(object sender, EventArgs e)
        {

            lblSubject.Text = this.title_text;
            lblMessage.Text = this.message_text;

        }

        private void btnOkay_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
