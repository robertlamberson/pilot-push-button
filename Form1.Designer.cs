﻿namespace PilotPushButton
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.grpSrc1 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc1 = new System.Windows.Forms.CheckBox();
            this.lblTtl1 = new System.Windows.Forms.Label();
            this.lblTtlSpc1 = new System.Windows.Forms.Label();
            this.lblSrc1Sz = new System.Windows.Forms.Label();
            this.lblSrc1Frmt = new System.Windows.Forms.Label();
            this.lblSrc1Name = new System.Windows.Forms.Label();
            this.grpSrc2 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc2 = new System.Windows.Forms.CheckBox();
            this.lblTtl2 = new System.Windows.Forms.Label();
            this.lblTtlSpc2 = new System.Windows.Forms.Label();
            this.lblSrc2Sz = new System.Windows.Forms.Label();
            this.lblSrc2Frmt = new System.Windows.Forms.Label();
            this.lblSrc2Name = new System.Windows.Forms.Label();
            this.grpSrc3 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc3 = new System.Windows.Forms.CheckBox();
            this.lblTtl3 = new System.Windows.Forms.Label();
            this.lblTtlSpc3 = new System.Windows.Forms.Label();
            this.lblSrc3Sz = new System.Windows.Forms.Label();
            this.lblSrc3Frmt = new System.Windows.Forms.Label();
            this.lblSrc3Name = new System.Windows.Forms.Label();
            this.grpSrc4 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc4 = new System.Windows.Forms.CheckBox();
            this.lblTtl4 = new System.Windows.Forms.Label();
            this.lblTtlSpc4 = new System.Windows.Forms.Label();
            this.lblSrc4Sz = new System.Windows.Forms.Label();
            this.lblSrc4Frmt = new System.Windows.Forms.Label();
            this.lblSrc4Name = new System.Windows.Forms.Label();
            this.grpSrc8 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc8 = new System.Windows.Forms.CheckBox();
            this.lblTtl8 = new System.Windows.Forms.Label();
            this.lblTtlSpc8 = new System.Windows.Forms.Label();
            this.lblSrc8Sz = new System.Windows.Forms.Label();
            this.lblSrc8Frmt = new System.Windows.Forms.Label();
            this.lblSrc8Name = new System.Windows.Forms.Label();
            this.grpSrc7 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc7 = new System.Windows.Forms.CheckBox();
            this.lblTtl7 = new System.Windows.Forms.Label();
            this.lblTtlSpc7 = new System.Windows.Forms.Label();
            this.lblSrc7Sz = new System.Windows.Forms.Label();
            this.lblSrc7Frmt = new System.Windows.Forms.Label();
            this.lblSrc7Name = new System.Windows.Forms.Label();
            this.grpSrc6 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc6 = new System.Windows.Forms.CheckBox();
            this.lblTtl6 = new System.Windows.Forms.Label();
            this.lblTtlSpc6 = new System.Windows.Forms.Label();
            this.lblSrc6Sz = new System.Windows.Forms.Label();
            this.lblSrc6Frmt = new System.Windows.Forms.Label();
            this.lblSrc6Name = new System.Windows.Forms.Label();
            this.grpSrc5 = new System.Windows.Forms.GroupBox();
            this.chkBoxSrc5 = new System.Windows.Forms.CheckBox();
            this.lblTtl5 = new System.Windows.Forms.Label();
            this.lblTtlSpc5 = new System.Windows.Forms.Label();
            this.lblSrc5Sz = new System.Windows.Forms.Label();
            this.lblSrc5Frmt = new System.Windows.Forms.Label();
            this.lblSrc5Name = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPush = new System.Windows.Forms.Button();
            this.StatusBox = new System.Windows.Forms.ListBox();
            this.grpSrc1.SuspendLayout();
            this.grpSrc2.SuspendLayout();
            this.grpSrc3.SuspendLayout();
            this.grpSrc4.SuspendLayout();
            this.grpSrc8.SuspendLayout();
            this.grpSrc7.SuspendLayout();
            this.grpSrc6.SuspendLayout();
            this.grpSrc5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSrc1
            // 
            this.grpSrc1.Controls.Add(this.chkBoxSrc1);
            this.grpSrc1.Controls.Add(this.lblTtl1);
            this.grpSrc1.Controls.Add(this.lblTtlSpc1);
            this.grpSrc1.Controls.Add(this.lblSrc1Sz);
            this.grpSrc1.Controls.Add(this.lblSrc1Frmt);
            this.grpSrc1.Controls.Add(this.lblSrc1Name);
            this.grpSrc1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc1.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc1.Location = new System.Drawing.Point(9, 22);
            this.grpSrc1.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc1.Name = "grpSrc1";
            this.grpSrc1.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc1.Size = new System.Drawing.Size(236, 199);
            this.grpSrc1.TabIndex = 9;
            this.grpSrc1.TabStop = false;
            this.grpSrc1.Text = "Source 1 Info";
            // 
            // chkBoxSrc1
            // 
            this.chkBoxSrc1.AutoSize = true;
            this.chkBoxSrc1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc1.Location = new System.Drawing.Point(7, 174);
            this.chkBoxSrc1.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc1.Name = "chkBoxSrc1";
            this.chkBoxSrc1.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc1.TabIndex = 5;
            this.chkBoxSrc1.Text = "Use This Source";
            this.chkBoxSrc1.UseVisualStyleBackColor = true;
            // 
            // lblTtl1
            // 
            this.lblTtl1.AutoSize = true;
            this.lblTtl1.Location = new System.Drawing.Point(16, 138);
            this.lblTtl1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl1.Name = "lblTtl1";
            this.lblTtl1.Size = new System.Drawing.Size(36, 14);
            this.lblTtl1.TabIndex = 4;
            this.lblTtl1.Text = "Total ";
            // 
            // lblTtlSpc1
            // 
            this.lblTtlSpc1.AutoSize = true;
            this.lblTtlSpc1.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc1.Name = "lblTtlSpc1";
            this.lblTtlSpc1.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc1.TabIndex = 3;
            this.lblTtlSpc1.Text = "Total Space Not available";
            // 
            // lblSrc1Sz
            // 
            this.lblSrc1Sz.AutoSize = true;
            this.lblSrc1Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc1Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc1Sz.Name = "lblSrc1Sz";
            this.lblSrc1Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc1Sz.TabIndex = 2;
            this.lblSrc1Sz.Text = "No Size Available";
            // 
            // lblSrc1Frmt
            // 
            this.lblSrc1Frmt.AutoSize = true;
            this.lblSrc1Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc1Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc1Frmt.Name = "lblSrc1Frmt";
            this.lblSrc1Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc1Frmt.TabIndex = 1;
            this.lblSrc1Frmt.Text = "Format Not Available";
            // 
            // lblSrc1Name
            // 
            this.lblSrc1Name.AutoSize = true;
            this.lblSrc1Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc1Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc1Name.Name = "lblSrc1Name";
            this.lblSrc1Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc1Name.TabIndex = 0;
            this.lblSrc1Name.Text = "No Name Available";
            // 
            // grpSrc2
            // 
            this.grpSrc2.Controls.Add(this.chkBoxSrc2);
            this.grpSrc2.Controls.Add(this.lblTtl2);
            this.grpSrc2.Controls.Add(this.lblTtlSpc2);
            this.grpSrc2.Controls.Add(this.lblSrc2Sz);
            this.grpSrc2.Controls.Add(this.lblSrc2Frmt);
            this.grpSrc2.Controls.Add(this.lblSrc2Name);
            this.grpSrc2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc2.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc2.Location = new System.Drawing.Point(250, 22);
            this.grpSrc2.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc2.Name = "grpSrc2";
            this.grpSrc2.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc2.Size = new System.Drawing.Size(261, 199);
            this.grpSrc2.TabIndex = 10;
            this.grpSrc2.TabStop = false;
            this.grpSrc2.Text = "Source 2 Info";
            // 
            // chkBoxSrc2
            // 
            this.chkBoxSrc2.AutoSize = true;
            this.chkBoxSrc2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc2.Location = new System.Drawing.Point(5, 174);
            this.chkBoxSrc2.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc2.Name = "chkBoxSrc2";
            this.chkBoxSrc2.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc2.TabIndex = 5;
            this.chkBoxSrc2.Text = "Use This Source";
            this.chkBoxSrc2.UseVisualStyleBackColor = true;
            // 
            // lblTtl2
            // 
            this.lblTtl2.AutoSize = true;
            this.lblTtl2.Location = new System.Drawing.Point(16, 138);
            this.lblTtl2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl2.Name = "lblTtl2";
            this.lblTtl2.Size = new System.Drawing.Size(36, 14);
            this.lblTtl2.TabIndex = 4;
            this.lblTtl2.Text = "Total ";
            // 
            // lblTtlSpc2
            // 
            this.lblTtlSpc2.AutoSize = true;
            this.lblTtlSpc2.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc2.Name = "lblTtlSpc2";
            this.lblTtlSpc2.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc2.TabIndex = 3;
            this.lblTtlSpc2.Text = "Total Space Not available";
            // 
            // lblSrc2Sz
            // 
            this.lblSrc2Sz.AutoSize = true;
            this.lblSrc2Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc2Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc2Sz.Name = "lblSrc2Sz";
            this.lblSrc2Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc2Sz.TabIndex = 2;
            this.lblSrc2Sz.Text = "No Size Available";
            // 
            // lblSrc2Frmt
            // 
            this.lblSrc2Frmt.AutoSize = true;
            this.lblSrc2Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc2Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc2Frmt.Name = "lblSrc2Frmt";
            this.lblSrc2Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc2Frmt.TabIndex = 1;
            this.lblSrc2Frmt.Text = "Format Not Available";
            // 
            // lblSrc2Name
            // 
            this.lblSrc2Name.AutoSize = true;
            this.lblSrc2Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc2Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc2Name.Name = "lblSrc2Name";
            this.lblSrc2Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc2Name.TabIndex = 0;
            this.lblSrc2Name.Text = "No Name Available";
            // 
            // grpSrc3
            // 
            this.grpSrc3.Controls.Add(this.chkBoxSrc3);
            this.grpSrc3.Controls.Add(this.lblTtl3);
            this.grpSrc3.Controls.Add(this.lblTtlSpc3);
            this.grpSrc3.Controls.Add(this.lblSrc3Sz);
            this.grpSrc3.Controls.Add(this.lblSrc3Frmt);
            this.grpSrc3.Controls.Add(this.lblSrc3Name);
            this.grpSrc3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc3.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc3.Location = new System.Drawing.Point(516, 22);
            this.grpSrc3.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc3.Name = "grpSrc3";
            this.grpSrc3.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc3.Size = new System.Drawing.Size(240, 199);
            this.grpSrc3.TabIndex = 11;
            this.grpSrc3.TabStop = false;
            this.grpSrc3.Text = "Source 3 Info";
            // 
            // chkBoxSrc3
            // 
            this.chkBoxSrc3.AutoSize = true;
            this.chkBoxSrc3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc3.Location = new System.Drawing.Point(7, 174);
            this.chkBoxSrc3.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc3.Name = "chkBoxSrc3";
            this.chkBoxSrc3.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc3.TabIndex = 5;
            this.chkBoxSrc3.Text = "Use This Source";
            this.chkBoxSrc3.UseVisualStyleBackColor = true;
            // 
            // lblTtl3
            // 
            this.lblTtl3.AutoSize = true;
            this.lblTtl3.Location = new System.Drawing.Point(16, 138);
            this.lblTtl3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl3.Name = "lblTtl3";
            this.lblTtl3.Size = new System.Drawing.Size(36, 14);
            this.lblTtl3.TabIndex = 4;
            this.lblTtl3.Text = "Total ";
            // 
            // lblTtlSpc3
            // 
            this.lblTtlSpc3.AutoSize = true;
            this.lblTtlSpc3.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc3.Name = "lblTtlSpc3";
            this.lblTtlSpc3.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc3.TabIndex = 3;
            this.lblTtlSpc3.Text = "Total Space Not available";
            // 
            // lblSrc3Sz
            // 
            this.lblSrc3Sz.AutoSize = true;
            this.lblSrc3Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc3Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc3Sz.Name = "lblSrc3Sz";
            this.lblSrc3Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc3Sz.TabIndex = 2;
            this.lblSrc3Sz.Text = "No Size Available";
            // 
            // lblSrc3Frmt
            // 
            this.lblSrc3Frmt.AutoSize = true;
            this.lblSrc3Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc3Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc3Frmt.Name = "lblSrc3Frmt";
            this.lblSrc3Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc3Frmt.TabIndex = 1;
            this.lblSrc3Frmt.Text = "Format Not Available";
            // 
            // lblSrc3Name
            // 
            this.lblSrc3Name.AutoSize = true;
            this.lblSrc3Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc3Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc3Name.Name = "lblSrc3Name";
            this.lblSrc3Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc3Name.TabIndex = 0;
            this.lblSrc3Name.Text = "No Name Available";
            // 
            // grpSrc4
            // 
            this.grpSrc4.Controls.Add(this.chkBoxSrc4);
            this.grpSrc4.Controls.Add(this.lblTtl4);
            this.grpSrc4.Controls.Add(this.lblTtlSpc4);
            this.grpSrc4.Controls.Add(this.lblSrc4Sz);
            this.grpSrc4.Controls.Add(this.lblSrc4Frmt);
            this.grpSrc4.Controls.Add(this.lblSrc4Name);
            this.grpSrc4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc4.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc4.Location = new System.Drawing.Point(759, 22);
            this.grpSrc4.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc4.Name = "grpSrc4";
            this.grpSrc4.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc4.Size = new System.Drawing.Size(247, 199);
            this.grpSrc4.TabIndex = 12;
            this.grpSrc4.TabStop = false;
            this.grpSrc4.Text = "Source 4 Info";
            this.grpSrc4.Enter += new System.EventHandler(this.grpSrc4_Enter);
            // 
            // chkBoxSrc4
            // 
            this.chkBoxSrc4.AutoSize = true;
            this.chkBoxSrc4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc4.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc4.Location = new System.Drawing.Point(5, 174);
            this.chkBoxSrc4.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc4.Name = "chkBoxSrc4";
            this.chkBoxSrc4.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc4.TabIndex = 5;
            this.chkBoxSrc4.Text = "Use This Source";
            this.chkBoxSrc4.UseVisualStyleBackColor = true;
            // 
            // lblTtl4
            // 
            this.lblTtl4.AutoSize = true;
            this.lblTtl4.Location = new System.Drawing.Point(16, 138);
            this.lblTtl4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl4.Name = "lblTtl4";
            this.lblTtl4.Size = new System.Drawing.Size(36, 14);
            this.lblTtl4.TabIndex = 4;
            this.lblTtl4.Text = "Total ";
            // 
            // lblTtlSpc4
            // 
            this.lblTtlSpc4.AutoSize = true;
            this.lblTtlSpc4.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc4.Name = "lblTtlSpc4";
            this.lblTtlSpc4.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc4.TabIndex = 3;
            this.lblTtlSpc4.Text = "Total Space Not available";
            // 
            // lblSrc4Sz
            // 
            this.lblSrc4Sz.AutoSize = true;
            this.lblSrc4Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc4Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc4Sz.Name = "lblSrc4Sz";
            this.lblSrc4Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc4Sz.TabIndex = 2;
            this.lblSrc4Sz.Text = "No Size Available";
            // 
            // lblSrc4Frmt
            // 
            this.lblSrc4Frmt.AutoSize = true;
            this.lblSrc4Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc4Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc4Frmt.Name = "lblSrc4Frmt";
            this.lblSrc4Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc4Frmt.TabIndex = 1;
            this.lblSrc4Frmt.Text = "Format Not Available";
            // 
            // lblSrc4Name
            // 
            this.lblSrc4Name.AutoSize = true;
            this.lblSrc4Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc4Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc4Name.Name = "lblSrc4Name";
            this.lblSrc4Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc4Name.TabIndex = 0;
            this.lblSrc4Name.Text = "No Name Available";
            // 
            // grpSrc8
            // 
            this.grpSrc8.Controls.Add(this.chkBoxSrc8);
            this.grpSrc8.Controls.Add(this.lblTtl8);
            this.grpSrc8.Controls.Add(this.lblTtlSpc8);
            this.grpSrc8.Controls.Add(this.lblSrc8Sz);
            this.grpSrc8.Controls.Add(this.lblSrc8Frmt);
            this.grpSrc8.Controls.Add(this.lblSrc8Name);
            this.grpSrc8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc8.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc8.Location = new System.Drawing.Point(759, 225);
            this.grpSrc8.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc8.Name = "grpSrc8";
            this.grpSrc8.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc8.Size = new System.Drawing.Size(247, 191);
            this.grpSrc8.TabIndex = 16;
            this.grpSrc8.TabStop = false;
            this.grpSrc8.Text = "Source 8 Info";
            // 
            // chkBoxSrc8
            // 
            this.chkBoxSrc8.AutoSize = true;
            this.chkBoxSrc8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc8.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc8.Location = new System.Drawing.Point(5, 168);
            this.chkBoxSrc8.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc8.Name = "chkBoxSrc8";
            this.chkBoxSrc8.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc8.TabIndex = 5;
            this.chkBoxSrc8.Text = "Use This Source";
            this.chkBoxSrc8.UseVisualStyleBackColor = true;
            // 
            // lblTtl8
            // 
            this.lblTtl8.AutoSize = true;
            this.lblTtl8.Location = new System.Drawing.Point(16, 138);
            this.lblTtl8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl8.Name = "lblTtl8";
            this.lblTtl8.Size = new System.Drawing.Size(36, 14);
            this.lblTtl8.TabIndex = 4;
            this.lblTtl8.Text = "Total ";
            // 
            // lblTtlSpc8
            // 
            this.lblTtlSpc8.AutoSize = true;
            this.lblTtlSpc8.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc8.Name = "lblTtlSpc8";
            this.lblTtlSpc8.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc8.TabIndex = 3;
            this.lblTtlSpc8.Text = "Total Space Not available";
            // 
            // lblSrc8Sz
            // 
            this.lblSrc8Sz.AutoSize = true;
            this.lblSrc8Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc8Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc8Sz.Name = "lblSrc8Sz";
            this.lblSrc8Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc8Sz.TabIndex = 2;
            this.lblSrc8Sz.Text = "No Size Available";
            // 
            // lblSrc8Frmt
            // 
            this.lblSrc8Frmt.AutoSize = true;
            this.lblSrc8Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc8Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc8Frmt.Name = "lblSrc8Frmt";
            this.lblSrc8Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc8Frmt.TabIndex = 1;
            this.lblSrc8Frmt.Text = "Format Not Available";
            // 
            // lblSrc8Name
            // 
            this.lblSrc8Name.AutoSize = true;
            this.lblSrc8Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc8Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc8Name.Name = "lblSrc8Name";
            this.lblSrc8Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc8Name.TabIndex = 0;
            this.lblSrc8Name.Text = "No Name Available";
            // 
            // grpSrc7
            // 
            this.grpSrc7.Controls.Add(this.chkBoxSrc7);
            this.grpSrc7.Controls.Add(this.lblTtl7);
            this.grpSrc7.Controls.Add(this.lblTtlSpc7);
            this.grpSrc7.Controls.Add(this.lblSrc7Sz);
            this.grpSrc7.Controls.Add(this.lblSrc7Frmt);
            this.grpSrc7.Controls.Add(this.lblSrc7Name);
            this.grpSrc7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc7.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc7.Location = new System.Drawing.Point(516, 225);
            this.grpSrc7.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc7.Name = "grpSrc7";
            this.grpSrc7.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc7.Size = new System.Drawing.Size(240, 191);
            this.grpSrc7.TabIndex = 15;
            this.grpSrc7.TabStop = false;
            this.grpSrc7.Text = "Source 7 Info";
            // 
            // chkBoxSrc7
            // 
            this.chkBoxSrc7.AutoSize = true;
            this.chkBoxSrc7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc7.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc7.Location = new System.Drawing.Point(7, 168);
            this.chkBoxSrc7.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc7.Name = "chkBoxSrc7";
            this.chkBoxSrc7.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc7.TabIndex = 5;
            this.chkBoxSrc7.Text = "Use This Source";
            this.chkBoxSrc7.UseVisualStyleBackColor = true;
            // 
            // lblTtl7
            // 
            this.lblTtl7.AutoSize = true;
            this.lblTtl7.Location = new System.Drawing.Point(16, 138);
            this.lblTtl7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl7.Name = "lblTtl7";
            this.lblTtl7.Size = new System.Drawing.Size(36, 14);
            this.lblTtl7.TabIndex = 4;
            this.lblTtl7.Text = "Total ";
            // 
            // lblTtlSpc7
            // 
            this.lblTtlSpc7.AutoSize = true;
            this.lblTtlSpc7.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc7.Name = "lblTtlSpc7";
            this.lblTtlSpc7.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc7.TabIndex = 3;
            this.lblTtlSpc7.Text = "Total Space Not available";
            // 
            // lblSrc7Sz
            // 
            this.lblSrc7Sz.AutoSize = true;
            this.lblSrc7Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc7Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc7Sz.Name = "lblSrc7Sz";
            this.lblSrc7Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc7Sz.TabIndex = 2;
            this.lblSrc7Sz.Text = "No Size Available";
            // 
            // lblSrc7Frmt
            // 
            this.lblSrc7Frmt.AutoSize = true;
            this.lblSrc7Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc7Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc7Frmt.Name = "lblSrc7Frmt";
            this.lblSrc7Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc7Frmt.TabIndex = 1;
            this.lblSrc7Frmt.Text = "Format Not Available";
            // 
            // lblSrc7Name
            // 
            this.lblSrc7Name.AutoSize = true;
            this.lblSrc7Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc7Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc7Name.Name = "lblSrc7Name";
            this.lblSrc7Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc7Name.TabIndex = 0;
            this.lblSrc7Name.Text = "No Name Available";
            // 
            // grpSrc6
            // 
            this.grpSrc6.Controls.Add(this.chkBoxSrc6);
            this.grpSrc6.Controls.Add(this.lblTtl6);
            this.grpSrc6.Controls.Add(this.lblTtlSpc6);
            this.grpSrc6.Controls.Add(this.lblSrc6Sz);
            this.grpSrc6.Controls.Add(this.lblSrc6Frmt);
            this.grpSrc6.Controls.Add(this.lblSrc6Name);
            this.grpSrc6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc6.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc6.Location = new System.Drawing.Point(250, 225);
            this.grpSrc6.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc6.Name = "grpSrc6";
            this.grpSrc6.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc6.Size = new System.Drawing.Size(261, 191);
            this.grpSrc6.TabIndex = 14;
            this.grpSrc6.TabStop = false;
            this.grpSrc6.Text = "Source 6 Info";
            // 
            // chkBoxSrc6
            // 
            this.chkBoxSrc6.AutoSize = true;
            this.chkBoxSrc6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc6.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc6.Location = new System.Drawing.Point(7, 168);
            this.chkBoxSrc6.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc6.Name = "chkBoxSrc6";
            this.chkBoxSrc6.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc6.TabIndex = 5;
            this.chkBoxSrc6.Text = "Use This Source";
            this.chkBoxSrc6.UseVisualStyleBackColor = true;
            // 
            // lblTtl6
            // 
            this.lblTtl6.AutoSize = true;
            this.lblTtl6.Location = new System.Drawing.Point(16, 138);
            this.lblTtl6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl6.Name = "lblTtl6";
            this.lblTtl6.Size = new System.Drawing.Size(36, 14);
            this.lblTtl6.TabIndex = 4;
            this.lblTtl6.Text = "Total ";
            // 
            // lblTtlSpc6
            // 
            this.lblTtlSpc6.AutoSize = true;
            this.lblTtlSpc6.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc6.Name = "lblTtlSpc6";
            this.lblTtlSpc6.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc6.TabIndex = 3;
            this.lblTtlSpc6.Text = "Total Space Not available";
            // 
            // lblSrc6Sz
            // 
            this.lblSrc6Sz.AutoSize = true;
            this.lblSrc6Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc6Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc6Sz.Name = "lblSrc6Sz";
            this.lblSrc6Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc6Sz.TabIndex = 2;
            this.lblSrc6Sz.Text = "No Size Available";
            // 
            // lblSrc6Frmt
            // 
            this.lblSrc6Frmt.AutoSize = true;
            this.lblSrc6Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc6Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc6Frmt.Name = "lblSrc6Frmt";
            this.lblSrc6Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc6Frmt.TabIndex = 1;
            this.lblSrc6Frmt.Text = "Format Not Available";
            // 
            // lblSrc6Name
            // 
            this.lblSrc6Name.AutoSize = true;
            this.lblSrc6Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc6Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc6Name.Name = "lblSrc6Name";
            this.lblSrc6Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc6Name.TabIndex = 0;
            this.lblSrc6Name.Text = "No Name Available";
            // 
            // grpSrc5
            // 
            this.grpSrc5.Controls.Add(this.chkBoxSrc5);
            this.grpSrc5.Controls.Add(this.lblTtl5);
            this.grpSrc5.Controls.Add(this.lblTtlSpc5);
            this.grpSrc5.Controls.Add(this.lblSrc5Sz);
            this.grpSrc5.Controls.Add(this.lblSrc5Frmt);
            this.grpSrc5.Controls.Add(this.lblSrc5Name);
            this.grpSrc5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpSrc5.Font = new System.Drawing.Font("Microsoft Yi Baiti", 8.2105F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.grpSrc5.Location = new System.Drawing.Point(9, 225);
            this.grpSrc5.Margin = new System.Windows.Forms.Padding(2);
            this.grpSrc5.Name = "grpSrc5";
            this.grpSrc5.Padding = new System.Windows.Forms.Padding(2);
            this.grpSrc5.Size = new System.Drawing.Size(236, 191);
            this.grpSrc5.TabIndex = 13;
            this.grpSrc5.TabStop = false;
            this.grpSrc5.Text = "Source 5 Info";
            // 
            // chkBoxSrc5
            // 
            this.chkBoxSrc5.AutoSize = true;
            this.chkBoxSrc5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkBoxSrc5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSrc5.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkBoxSrc5.Location = new System.Drawing.Point(7, 168);
            this.chkBoxSrc5.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoxSrc5.Name = "chkBoxSrc5";
            this.chkBoxSrc5.Size = new System.Drawing.Size(142, 24);
            this.chkBoxSrc5.TabIndex = 5;
            this.chkBoxSrc5.Text = "Use This Source";
            this.chkBoxSrc5.UseVisualStyleBackColor = true;
            // 
            // lblTtl5
            // 
            this.lblTtl5.AutoSize = true;
            this.lblTtl5.Location = new System.Drawing.Point(16, 138);
            this.lblTtl5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtl5.Name = "lblTtl5";
            this.lblTtl5.Size = new System.Drawing.Size(36, 14);
            this.lblTtl5.TabIndex = 4;
            this.lblTtl5.Text = "Total ";
            // 
            // lblTtlSpc5
            // 
            this.lblTtlSpc5.AutoSize = true;
            this.lblTtlSpc5.Location = new System.Drawing.Point(16, 111);
            this.lblTtlSpc5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTtlSpc5.Name = "lblTtlSpc5";
            this.lblTtlSpc5.Size = new System.Drawing.Size(140, 14);
            this.lblTtlSpc5.TabIndex = 3;
            this.lblTtlSpc5.Text = "Total Space Not available";
            // 
            // lblSrc5Sz
            // 
            this.lblSrc5Sz.AutoSize = true;
            this.lblSrc5Sz.Location = new System.Drawing.Point(16, 85);
            this.lblSrc5Sz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc5Sz.Name = "lblSrc5Sz";
            this.lblSrc5Sz.Size = new System.Drawing.Size(99, 14);
            this.lblSrc5Sz.TabIndex = 2;
            this.lblSrc5Sz.Text = "No Size Available";
            // 
            // lblSrc5Frmt
            // 
            this.lblSrc5Frmt.AutoSize = true;
            this.lblSrc5Frmt.Location = new System.Drawing.Point(16, 56);
            this.lblSrc5Frmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc5Frmt.Name = "lblSrc5Frmt";
            this.lblSrc5Frmt.Size = new System.Drawing.Size(119, 14);
            this.lblSrc5Frmt.TabIndex = 1;
            this.lblSrc5Frmt.Text = "Format Not Available";
            // 
            // lblSrc5Name
            // 
            this.lblSrc5Name.AutoSize = true;
            this.lblSrc5Name.Location = new System.Drawing.Point(16, 25);
            this.lblSrc5Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSrc5Name.Name = "lblSrc5Name";
            this.lblSrc5Name.Size = new System.Drawing.Size(107, 14);
            this.lblSrc5Name.TabIndex = 0;
            this.lblSrc5Name.Text = "No Name Available";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.menuStrip1.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1023, 25);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.configToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.directoriesToolStripMenuItem});
            this.configToolStripMenuItem.Font = new System.Drawing.Font("Source Code Pro Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.configToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.configToolStripMenuItem.Text = "[ CONFIG ]";
            // 
            // directoriesToolStripMenuItem
            // 
            this.directoriesToolStripMenuItem.Name = "directoriesToolStripMenuItem";
            this.directoriesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.directoriesToolStripMenuItem.Text = "Directories";
            this.directoriesToolStripMenuItem.Click += new System.EventHandler(this.directoriesToolStripMenuItem_Click);
            // 
            // btnPush
            // 
            this.btnPush.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPush.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPush.Font = new System.Drawing.Font("Liberation Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPush.Location = new System.Drawing.Point(838, 421);
            this.btnPush.Margin = new System.Windows.Forms.Padding(2);
            this.btnPush.Name = "btnPush";
            this.btnPush.Size = new System.Drawing.Size(166, 45);
            this.btnPush.TabIndex = 18;
            this.btnPush.Text = "Pilot Push This Button";
            this.btnPush.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnPush.UseVisualStyleBackColor = false;
            this.btnPush.Click += new System.EventHandler(this.btnPush_Click);
            // 
            // StatusBox
            // 
            this.StatusBox.BackColor = System.Drawing.Color.PowderBlue;
            this.StatusBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StatusBox.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBox.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.StatusBox.FormattingEnabled = true;
            this.StatusBox.ItemHeight = 15;
            this.StatusBox.Location = new System.Drawing.Point(9, 421);
            this.StatusBox.Margin = new System.Windows.Forms.Padding(2);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(818, 45);
            this.StatusBox.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(1023, 477);
            this.Controls.Add(this.StatusBox);
            this.Controls.Add(this.btnPush);
            this.Controls.Add(this.grpSrc8);
            this.Controls.Add(this.grpSrc7);
            this.Controls.Add(this.grpSrc6);
            this.Controls.Add(this.grpSrc5);
            this.Controls.Add(this.grpSrc4);
            this.Controls.Add(this.grpSrc3);
            this.Controls.Add(this.grpSrc2);
            this.Controls.Add(this.grpSrc1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.ForeColor = System.Drawing.Color.PowderBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Pilot Push Button";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpSrc1.ResumeLayout(false);
            this.grpSrc1.PerformLayout();
            this.grpSrc2.ResumeLayout(false);
            this.grpSrc2.PerformLayout();
            this.grpSrc3.ResumeLayout(false);
            this.grpSrc3.PerformLayout();
            this.grpSrc4.ResumeLayout(false);
            this.grpSrc4.PerformLayout();
            this.grpSrc8.ResumeLayout(false);
            this.grpSrc8.PerformLayout();
            this.grpSrc7.ResumeLayout(false);
            this.grpSrc7.PerformLayout();
            this.grpSrc6.ResumeLayout(false);
            this.grpSrc6.PerformLayout();
            this.grpSrc5.ResumeLayout(false);
            this.grpSrc5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox grpSrc1;
        private System.Windows.Forms.Label lblSrc1Name;
        private System.Windows.Forms.Label lblSrc1Sz;
        private System.Windows.Forms.Label lblSrc1Frmt;
        private System.Windows.Forms.Label lblTtl1;
        private System.Windows.Forms.Label lblTtlSpc1;
        private System.Windows.Forms.CheckBox chkBoxSrc1;
        private System.Windows.Forms.GroupBox grpSrc2;
        private System.Windows.Forms.CheckBox chkBoxSrc2;
        private System.Windows.Forms.Label lblTtl2;
        private System.Windows.Forms.Label lblTtlSpc2;
        private System.Windows.Forms.Label lblSrc2Sz;
        private System.Windows.Forms.Label lblSrc2Frmt;
        private System.Windows.Forms.Label lblSrc2Name;
        private System.Windows.Forms.GroupBox grpSrc3;
        private System.Windows.Forms.CheckBox chkBoxSrc3;
        private System.Windows.Forms.Label lblTtl3;
        private System.Windows.Forms.Label lblTtlSpc3;
        private System.Windows.Forms.Label lblSrc3Sz;
        private System.Windows.Forms.Label lblSrc3Frmt;
        private System.Windows.Forms.Label lblSrc3Name;
        private System.Windows.Forms.GroupBox grpSrc4;
        private System.Windows.Forms.CheckBox chkBoxSrc4;
        private System.Windows.Forms.Label lblTtl4;
        private System.Windows.Forms.Label lblTtlSpc4;
        private System.Windows.Forms.Label lblSrc4Sz;
        private System.Windows.Forms.Label lblSrc4Frmt;
        private System.Windows.Forms.Label lblSrc4Name;
        private System.Windows.Forms.GroupBox grpSrc8;
        private System.Windows.Forms.CheckBox chkBoxSrc8;
        private System.Windows.Forms.Label lblTtl8;
        private System.Windows.Forms.Label lblTtlSpc8;
        private System.Windows.Forms.Label lblSrc8Sz;
        private System.Windows.Forms.Label lblSrc8Frmt;
        private System.Windows.Forms.Label lblSrc8Name;
        private System.Windows.Forms.GroupBox grpSrc7;
        private System.Windows.Forms.CheckBox chkBoxSrc7;
        private System.Windows.Forms.Label lblTtl7;
        private System.Windows.Forms.Label lblTtlSpc7;
        private System.Windows.Forms.Label lblSrc7Sz;
        private System.Windows.Forms.Label lblSrc7Frmt;
        private System.Windows.Forms.Label lblSrc7Name;
        private System.Windows.Forms.GroupBox grpSrc6;
        private System.Windows.Forms.CheckBox chkBoxSrc6;
        private System.Windows.Forms.Label lblTtl6;
        private System.Windows.Forms.Label lblTtlSpc6;
        private System.Windows.Forms.Label lblSrc6Sz;
        private System.Windows.Forms.Label lblSrc6Frmt;
        private System.Windows.Forms.Label lblSrc6Name;
        private System.Windows.Forms.GroupBox grpSrc5;
        private System.Windows.Forms.CheckBox chkBoxSrc5;
        private System.Windows.Forms.Label lblTtl5;
        private System.Windows.Forms.Label lblTtlSpc5;
        private System.Windows.Forms.Label lblSrc5Sz;
        private System.Windows.Forms.Label lblSrc5Frmt;
        private System.Windows.Forms.Label lblSrc5Name;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem directoriesToolStripMenuItem;
        private System.Windows.Forms.Button btnPush;
        private System.Windows.Forms.ListBox StatusBox;
    }
}

