﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Xml;


namespace PilotPushButton
{
    public partial class Form1 : Form
    {
        public storage_card[] raw_drives_list;

        public string[] chkbxs;

        public string currentSource;

        public string currentTarget;

        private string config_path;

        public List<string> destination_directories;

        public string current_source_drive;

        public int current_source_count;

        public string current_destination_folder;

        public string current_destination_count;

        public int current_transfer_counter;

        public int current_transfer_skipper;

        public string current_end_statement;

        public Form1()
        {
            InitializeComponent();

            chkBoxSrc1.Visible = false;
            chkBoxSrc2.Visible = false;
            chkBoxSrc3.Visible = false;
            chkBoxSrc4.Visible = false;
            chkBoxSrc5.Visible = false;
            chkBoxSrc6.Visible = false;
            chkBoxSrc7.Visible = false;
            chkBoxSrc8.Visible = false;


        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.initDriveReader();

            this.destination_directories = new List<string>();

            this.config_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//ppb_config.xml";

            this.initDestinationConfig();

        }

        private void initDestinationConfig()
        {

            XmlTextReader reader = new XmlTextReader(this.config_path);

            if (reader.ReadToFollowing("destination_dir_names"))
            {
                XmlReader r = reader.ReadSubtree();

                while (r.Read())
                {
                    if (r.NodeType == XmlNodeType.Element || r.NodeType == XmlNodeType.Text || r.NodeType == XmlNodeType.EndElement)
                    {
                        if (r.Name != string.Empty)
                        {

                        }
                        if (r.Value != string.Empty)
                        {

                            this.destination_directories.Add(r.Value);

                        }
                    }

                }

                r.Close();

            }

            reader.Close();
            
        }

        private void initDriveReader()
        {
            int countr = 1;

            pilot_terminal_computer PTC = new pilot_terminal_computer();

            this.raw_drives_list = PTC.getListDrives();

            foreach (var source in this.raw_drives_list)
            {

                if (countr == 1)
                {

                    grpSrc1.Text = source.textName;

                    lblSrc1Name.Text = source.driveName;

                    lblSrc1Frmt.Text = source.driveFormat;

                    lblSrc1Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc1.Text = FormatBytes(source.freeSpace);

                    lblTtl1.Text = FormatBytes(source.totalSize);

                    chkBoxSrc1.Visible = true;

                    chkBoxSrc1.Checked = true;

                }
                else if (countr == 2)
                {

                    grpSrc2.Text = source.textName;

                    lblSrc2Name.Text = source.driveName;

                    lblSrc2Frmt.Text = source.driveFormat;

                    lblSrc2Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc2.Text = FormatBytes(source.freeSpace);

                    lblTtl2.Text = FormatBytes(source.totalSize);

                    chkBoxSrc2.Visible = true;

                    chkBoxSrc2.Checked = true;
                }
                else if (countr == 3)
                {

                    grpSrc3.Text = source.textName;

                    lblSrc3Name.Text = source.driveName;

                    lblSrc3Frmt.Text = source.driveFormat;

                    lblSrc3Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc3.Text = FormatBytes(source.freeSpace);

                    lblTtl3.Text = FormatBytes(source.totalSize);

                    chkBoxSrc3.Visible = true;

                    chkBoxSrc3.Checked = true;
                }
                else if (countr == 4)
                {

                    grpSrc4.Text = source.textName;

                    lblSrc4Name.Text = source.driveName;

                    lblSrc4Frmt.Text = source.driveFormat;

                    lblSrc4Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc4.Text = FormatBytes(source.freeSpace);

                    lblTtl4.Text = FormatBytes(source.totalSize);

                    chkBoxSrc4.Visible = true;

                    chkBoxSrc4.Checked = true;
                }
                else if (countr == 5)
                {

                    grpSrc5.Text = source.textName;

                    lblSrc5Name.Text = source.driveName;

                    lblSrc5Frmt.Text = source.driveFormat;

                    lblSrc5Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc5.Text = FormatBytes(source.freeSpace);

                    lblTtl5.Text = FormatBytes(source.totalSize);

                    chkBoxSrc5.Visible = true;

                    chkBoxSrc5.Checked = true;
                }
                else if (countr == 6)
                {

                    grpSrc6.Text = source.textName;

                    lblSrc6Name.Text = source.driveName;

                    lblSrc6Frmt.Text = source.driveFormat;

                    lblSrc6Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc6.Text = FormatBytes(source.freeSpace);

                    lblTtl6.Text = FormatBytes(source.totalSize);

                    chkBoxSrc6.Visible = true;

                    chkBoxSrc6.Checked = true;
                }
                else if (countr == 7)
                {

                    grpSrc7.Text = source.textName;

                    lblSrc7Name.Text = source.driveName;

                    lblSrc7Frmt.Text = source.driveFormat;

                    lblSrc7Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc7.Text = FormatBytes(source.freeSpace);

                    lblTtl7.Text = FormatBytes(source.totalSize);

                    chkBoxSrc7.Visible = true;

                    chkBoxSrc7.Checked = true;
                }
                else if (countr == 8)
                {

                    grpSrc8.Text = source.textName;

                    lblSrc8Name.Text = source.driveName;

                    lblSrc8Frmt.Text = source.driveFormat;

                    lblSrc8Sz.Text = FormatBytes(source.totalSpace);

                    lblTtlSpc8.Text = FormatBytes(source.freeSpace);

                    lblTtl8.Text = FormatBytes(source.totalSize);

                    chkBoxSrc8.Visible = true;

                    chkBoxSrc8.Checked = true;
                }

                countr++;

            }


        }

        private static string FormatBytes(double bytes)
        {
            string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
            int i;
            double dblSByte = bytes;
            for (i = 0; i < Suffix.Length && bytes >= 1024; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }

            return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
        }

        private void directoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drive_configs dconfg = new drive_configs();
            dconfg.Show();
        }
        
        private void btnPush_Click(object sender, EventArgs e)
        {

            List<int>SourceDirectories = new List<int>();

            if ( chkBoxSrc1.Checked)
            {
                /*
                string ttl = "Sending Source 1 Content";
                string msg = "The content from source 1 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(1, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */
              //  int boxNumber = 1;
                SourceDirectories.Add(1);
            //    this.CopySourceFolderToCloudDrive( 1);
                
            }

            if( chkBoxSrc2.Checked)
            {
                /*
                string ttl = "Sending Source 2 Content";
                string msg = "The content from source 2 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(2, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */
                SourceDirectories.Add(2);

          //      this.CopySourceFolderToCloudDrive(2);

            }

            if (chkBoxSrc3.Checked)
            {
                /*
                string ttl = "Sending Source 2 Content";
                string msg = "The content from source 2 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(3, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */
                SourceDirectories.Add(3);

//                this.CopySourceFolderToCloudDrive(3);

            }

            if (chkBoxSrc4.Checked)
            {

                /*
                string ttl = "Sending Source 4 Content";
                string msg = "The content from source 4 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(4, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */

                SourceDirectories.Add(4);

            //    this.CopySourceFolderToCloudDrive(4);

            }

            if (chkBoxSrc5.Checked)
            {

                /*
                string ttl = "Sending Source 5 Content";
                string msg = "The content from source 5 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(5, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */

                SourceDirectories.Add(5);


            //    this.CopySourceFolderToCloudDrive(5);

            }

            if (chkBoxSrc6.Checked)
            {
                /*
                string ttl = "Sending Source 6 Content";
                string msg = "The content from source 6 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(6, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();

    */
                SourceDirectories.Add(6);



             //   this.CopySourceFolderToCloudDrive(6);

            }

            if (chkBoxSrc7.Checked)
            {
                /*
                string ttl = "Sending Source 7 Content";
                string msg = "The content from source 7 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(7, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */
                SourceDirectories.Add(7);



               // this.CopySourceFolderToCloudDrive(7);

            }

            if (chkBoxSrc8.Checked)
            {
                /*
                string ttl = "Sending Source 8 Content";
                string msg = "The content from source 8 is being sent to cloud sync drives.  Please find something else to do and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(8, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();
                */
                SourceDirectories.Add(8);



                //this.CopySourceFolderToCloudDrive(8);

            }

            foreach( int Source in SourceDirectories)
            {

                string ttl = "Sending Source " + Source + " Content";
                string msg = "Please do something else and check back shortly.  Thanks!";
                conf_trans confDiag = new conf_trans(Source, "before", ttl, msg);  // new Form(1, "before");
                confDiag.ShowDialog();


                this.CopySourceFolderToCloudDrive( Source );


            }



        }

        private void CopySourceFolderToCloudDrive( int SourceNumber )
        {
            DateTime now1 = new DateTime();

            now1 = DateTime.Today;

            string bugun = now1.ToString("yyyy-MM-dd");

            string ndrive = "";

            string text_info = "";
            
            switch( SourceNumber)
            {
                case 1:

                    text_info = lblSrc1Name.Text;
                    ndrive = lblSrc1Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 1 Transfer Complete - Refreshing Storage Media";

                    break;
                case 2:

                    text_info = lblSrc2Name.Text;
                    ndrive = lblSrc2Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 2 Transfer Complete - Refreshing Storage Media";

                    break;
                case 3:

                    text_info = lblSrc3Name.Text;
                    ndrive = lblSrc3Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 3 Transfer Complete - Refreshing Storage Media";

                    break;
                case 4:

                    text_info = lblSrc4Name.Text;
                    ndrive = lblSrc4Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 4 Transfer Complete - Refreshing Storage Media";

                    break;
                case 5:

                    text_info = lblSrc5Name.Text;
                    ndrive = lblSrc5Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 5 Transfer Complete - Refreshing Storage Media";

                    break;
                case 6:

                    text_info = lblSrc6Name.Text;
                    ndrive = lblSrc6Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 6 Transfer Complete - Refreshing Storage Media";

                    break;
                case 7:

                    text_info = lblSrc7Name.Text;
                    ndrive = lblSrc7Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 7 Transfer Complete - Refreshing Storage Media";

                    break;
                case 8:

                    text_info = lblSrc8Name.Text;
                    ndrive = lblSrc8Name.Text.Replace(":", "");
                    this.current_end_statement = "Image Source 8 Transfer Complete - Refreshing Storage Media";

                    break;
                    
            }
            

            ndrive = ndrive.Replace(@"\", string.Empty);

            string ndir = bugun + "\\" + ndrive;

            foreach (string destination in this.destination_directories)
            {
                string enddest = @destination + "\\" + ndir;

                Directory.CreateDirectory(enddest);

                this.current_transfer_skipper = 0;

                this.StartTransfer(text_info, enddest);

            }

        }

        private void FileStatus()
        {
            this.current_transfer_counter++;

            string ctc = this.current_transfer_counter.ToString();

            string msg = ctc + " out of " + this.current_source_count.ToString() + " completed for drive " + this.current_source_drive;

            StatusBox.Items.Add(msg);

            StatusBox.Refresh();

            StatusBox.SelectedIndex = StatusBox.Items.Count - 1;
            
          //  int srcc = Convert.ToInt32(this.current_source_count);

            if ( this.current_transfer_counter == this.current_source_count)
            {
               
                this.current_transfer_counter=0;

                StatusBox.Items.Add("Those ones are done, you might want to close the window if it hasn't started over yet, bitches...");

                StatusBox.Refresh();

            }

        }
        
        private void StartTransfer(string src, string dest)
        {
            this.current_transfer_counter = 0;

            this.current_source_drive = src;

            this.current_destination_folder = dest;
            
            this.CopyFolder(src, dest);
            
        }
        
        public void CopyFolder(string sourceFolder, string destFolder)
        {
            
            if (!Directory.Exists(destFolder))
            {

                Directory.CreateDirectory(destFolder);
                
            }
            string[] files = Directory.GetFiles(sourceFolder);
            //            this.current_source_count = files.Length.ToString();
            this.current_source_count = files.Length;

            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest);
                
                this.FileStatus();
                
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
            
        }

        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }

        private void grpSrc4_Enter(object sender, EventArgs e)
        {

        }
    }
}
