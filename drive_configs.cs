﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace PilotPushButton
{
    public partial class drive_configs : Form
    {

        public string config_path;

        public ppb_configuration conf;

        public drive_configs()
        {
            InitializeComponent();

            this.config_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//ppb_config.xml";

            this.conf = new ppb_configuration();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nwName = txtNewName.Text;

            if( nwName != "")
            {
                lstBxDriveNames.Items.Add(nwName);
                
            }
        }

        private void btnSaveFolder_Click(object sender, EventArgs e)
        {
            string New_Folder_Name = txtDestFolder.Text;

            if (New_Folder_Name != "")
            {
                lstBxDestDirs.Items.Add(New_Folder_Name);

            }

        }

        private void btnSv_Click(object sender, EventArgs e)
        {
            List<string> dirs = new List<string>();

            foreach ( string item in lstBxDriveNames.Items )
            {

                dirs.Add(item);
                
            }

            this.conf.source_dir_names = dirs.ToArray();

            List<string> dests = new List<string>();

            foreach( string item in lstBxDestDirs.Items )
            {

                dests.Add(item);

            }

            this.conf.destination_dir_names = dests.ToArray();

            System.Xml.Serialization.XmlSerializer writer =
                                    new System.Xml.Serialization.XmlSerializer(typeof(ppb_configuration));
            
            System.IO.FileStream file = System.IO.File.Create(this.config_path);
            
            writer.Serialize(file, this.conf);

            file.Close();

        }

        private void drive_configs_Load(object sender, EventArgs e)
        {
            XmlTextReader reader = new XmlTextReader(this.config_path);

            if (reader.ReadToFollowing("source_dir_names"))
            {
                XmlReader r = reader.ReadSubtree();

                while (r.Read())
                {
                    if (r.NodeType == XmlNodeType.Element || r.NodeType == XmlNodeType.Text || r.NodeType == XmlNodeType.EndElement)
                    {
                        if (r.Name != string.Empty)
                        {

                        }
                        if (r.Value != string.Empty)
                        {

                            lstBxDriveNames.Items.Add(r.Value);

                        }
                    }

                }

                r.Close();
                
            }
            
            if (reader.ReadToFollowing("destination_dir_names"))
            {
                XmlReader r = reader.ReadSubtree();

                while (r.Read())
                {
                    if (r.NodeType == XmlNodeType.Element || r.NodeType == XmlNodeType.Text || r.NodeType == XmlNodeType.EndElement)
                    {
                        if (r.Name != string.Empty)
                        {

                        }
                        if (r.Value != string.Empty)
                        {

                            lstBxDestDirs.Items.Add(r.Value);

                        }
                    }

                }

                r.Close();
                
            }
            
            reader.Close();

        }
        
    }
}
