﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PilotPushButton
{
    class pilot_terminal_computer
    {
        
        //public List<storage_card> cards;
        public storage_card[] storage_cards;
        
        public pilot_terminal_computer()
        {
           // this.drive_counter = 0;
        }

        public storage_card[] getListDrives()
        {

            List<storage_card> cards = new List<storage_card>();

            foreach (var drive in DriveInfo.GetDrives())
            {

                if (drive.IsReady == true)
                {
                    // Check volume name here
                    /*
List<int> termsList = new List<int>();
for (int runs = 0; runs < 400; runs++)
{
    termsList.Add(value);
}

// You can convert it back to an array if you would like to
int[] terms = termsList.ToArray();                     
                     
                     */
                    if ( File.Exists( drive.Name + @"\validity.txt") )
                    {

                        storage_card card = new storage_card();

                        card.driveName = drive.Name;
                        card.driveFormat = drive.DriveFormat;
                        card.volumeLabel = drive.VolumeLabel;
                        card.freeSpace = drive.TotalFreeSpace;
                        card.totalSpace = drive.TotalSize;
                        card.totalSize = drive.TotalSize;
                        card.textName = System.IO.File.ReadAllText(drive.Name + @"\validity.txt");

                        cards.Add(card);
                       // this.storage_cards[] = ;

                    }
                    
                }

            }
            
            storage_cards = cards.ToArray();
            
            System.Xml.Serialization.XmlSerializer writer =
                        new System.Xml.Serialization.XmlSerializer(typeof(storage_card[]));

            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//SerializationOverview.xml";

            System.IO.FileStream file = System.IO.File.Create(path);
            
            writer.Serialize(file, storage_cards);

            file.Close();
            
            return storage_cards;
            
        }
        


    }
}
